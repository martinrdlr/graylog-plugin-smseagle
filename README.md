SMSEagle Plugin for Graylog
=============================

An alarm callback plugin for integrating the [SMSEagle API](https://www.smseagle.eu/) into [Graylog](https://www.graylog.org/).

**Required Graylog version:** 2.0.0 and later

## Installation

## Build

This project is using Maven 3 and requires Java 8 or higher.

You can build the plugin (JAR) with `mvn package`.
